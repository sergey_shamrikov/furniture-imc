$(document).ready(function(){
  
  // first screen
  function firstScreen(){
    var windowHeight = $(window).height();
    $('.first_screen_box').height(windowHeight);
  }
  firstScreen();
  $(window).on('resize',firstScreen);

  // modal window
  $('.popup_open').on('click',function(){
  	var modal = $(this).attr('data-modal');
  	$(modal).arcticmodal();
  });

  // wow js
  wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
  wow.init();


  $(window).scroll(function(){
  	$('.scroll_info').animate({opacity:0},1000);
  });

});